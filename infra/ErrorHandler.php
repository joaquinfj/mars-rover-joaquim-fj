<?php

namespace infra;

/**
 * Class ErrorHandler
 * @package infra
 */
class ErrorHandler
{
    /** @var bool $exceptionHandling */
    protected static $exceptionHandling = true;
    /** @var  array $messages */
    protected static $messages;

    /**
     * @param boolean $exceptionHandling
     */
    public static function setExceptionHandling($exceptionHandling)
    {
        self::$exceptionHandling = $exceptionHandling;
    }

    /**
     * @param string $messageError
     * @return bool
     * @throws \Exception
     */
    public static function returnError($messageError)
    {
        self::$messages[] = $messageError;
        if (self::$exceptionHandling) {
            throw new \Exception(self::getMessages());
        } else {
            return false;
        }
    }

    /**
     * @return string
     */
    public static function getMessages()
    {
        if (count(self::$messages)>1) {
            $messages = join(PHP_EOL, self::$messages);
        } else {
            $messages = self::$messages[0];
        }

        return $messages;
    }
}
