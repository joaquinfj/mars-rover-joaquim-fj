<?php

namespace infra;

/**
 * Class OutputFormatter
 * @package infra
 */
class OutputFormatter
{
    const SPLIT_RESPONSES_CHAR = PHP_EOL;

    /**
     * @param array|string $responses
     * @return string
     */
    public function formatResponse($responses)
    {
        $responses = array_filter($responses);
        if (is_array($responses)) {
            $strResponse = join($this::SPLIT_RESPONSES_CHAR, $responses);
        } else {
            $strResponse = $responses;
        }

        return $strResponse;
    }

    /**
     * @param array $strXYO
     * @return string
     */
    public function formatRoverResponse($strXYO)
    {
        return implode(' ', $strXYO);
    }
}
