<?php

namespace logic\commands;

use infra\ErrorHandler;
use logic\mars\MarsElement;
use logic\mars\Plateau;
use logic\mars\Rover;

class AbsCommander
{
    const TYPE_ROVER_COMM = 'Rover';
    const TYPE_PLATEAU_COMM = 'Plateau';

    /** @var  array $commands */
    protected $commands;
    /** @var  MarsElement|Rover|Plateau $receiver */
    protected $receiver;

    /**
     * AbsCommander constructor.
     * @param MarsElement $receiver
     * @param array $commands
     */
    public function __construct($receiver = null, $commands = null)
    {
        $this->receiver = $receiver;
        $this->commands = $commands;
    }


    /**
     * @param string $typeOfCommand
     * @param string $commands
     * @param MarsElement|null $parent
     *
     * @return bool|PlateauCommander|RoverCommander
     */
    public function createCommander($typeOfCommand, $commands, $parent = null)
    {
        if ($typeOfCommand == $this::TYPE_ROVER_COMM) {
            $rover = new RoverCommander(new Rover(), $commands, $parent);
            $rover->getReceiver()->setPlateau($parent);
            return $rover;
        } elseif (($typeOfCommand == $this::TYPE_PLATEAU_COMM)) {
            return new PlateauCommander(new Plateau(), $commands);
        } else {
            return ErrorHandler::returnError('Type of group of commands invalid. No commander implemented');
        }
    }

    /**
     * @return bool|string
     */
    public function executeCommands()
    {
        $response = false;
        foreach ($this->commands as $command) {
            $response = $this->receiver->executeCommand($command);
        }

        return $response;
    }

    /**
     * @return MarsElement|Rover|Plateau
     */
    public function getReceiver()
    {
        return $this->receiver;
    }
}
