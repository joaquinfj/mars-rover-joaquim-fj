<?php

namespace logic\commands;

use infra\ErrorHandler;
use infra\OutputFormatter;

/**
 * Class InputParser
 * @package logic\commands
 */
class InputCommandParser
{
    const SPLIT_COMMANDER_CHAR = PHP_EOL;
    const SPLIT_LINE_CHAR = ' ';

    /**
     * InputParser constructor.
     */
    public function __construct()
    {
        return $this;
    }

    /**
     * @param string $txtCommands
     * @return string
     */
    public function processInputCommands($txtCommands)
    {
        $txtCommands = $this->parseArgsIntoTxt($txtCommands);

        if (!$this->validateInputCommands($txtCommands)) {
            return ErrorHandler::returnError('The input text received do not have the expected format.');
        }

        $lines = $this->parseTxtIntoLines($txtCommands);
        $commanders = $this->parseLinesIntoCommanders($lines);
        if ($commanders === false) {
            return ErrorHandler::returnError('The lines could not be parsed into proper commanders.');
        }

        $responses = $this->execute($commanders);
        if (!$responses) {
            $responses = ErrorHandler::getMessages();
        }
        $output = new OutputFormatter();
        return $output->formatResponse($responses);
    }

    /**
     * @param mixed $args
     * @return string
     */
    protected function parseArgsIntoTxt($args)
    {
        $txtCommands = $args;
        if (is_array($args)) {
            $txtCommands = join(self::SPLIT_LINE_CHAR, $args);
        }

        return $txtCommands;
    }

    /**
     * @param string $txtCommands
     * @return array
     */
    protected function parseTxtIntoLines($txtCommands)
    {
        $aLines = explode(self::SPLIT_COMMANDER_CHAR, $txtCommands);
        return $aLines;
    }

    /**
     * @param array $lines
     * @return AbsCommander[]
     */
    protected function parseLinesIntoCommanders($lines)
    {
        $aCommanders = array();

        $rawPlateauComm0 = array_shift($lines);
        $plateauCdr = new AbsCommander();
        $plateauCdr = $plateauCdr->createCommander($plateauCdr::TYPE_PLATEAU_COMM, [$rawPlateauComm0]);
        $aCommanders[] = $plateauCdr;

        $aRawRoversGroupCommands = array_chunk($lines, RoverCommander::GROUP_COMMANDS);
        foreach ($aRawRoversGroupCommands as $aGroupCommand) {
            $roverCdr = new AbsCommander();
            $roverCdr = $roverCdr->createCommander(
                $roverCdr::TYPE_ROVER_COMM,
                $aGroupCommand,
                $plateauCdr->getReceiver()
            );
            $aCommanders[] = $roverCdr;
        }

        return $aCommanders;
    }

    /**
     * @param string $txtCommands
     * @return bool
     */
    protected function validateInputCommands($txtCommands)
    {
        $elements = explode(self::SPLIT_COMMANDER_CHAR, $txtCommands);
        if (count($elements) >= 3 && ((count($elements) - 1) % 2) === 0) {
            return true;
        }

        return false;
    }

    /**
     * @param AbsCommander[] $commanders
     * @return array|bool
     */
    protected function execute($commanders)
    {
        $responses = array();
        foreach ($commanders as $commander) {
            $response = $commander->executeCommands();
            if ($response !== false) {
                $responses[] = $response;
            } else {
                return ErrorHandler::returnError('The commands could not be executed.');
            }
        }

        return $responses;
    }
}
