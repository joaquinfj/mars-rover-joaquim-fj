<?php

namespace logic\commands;

/**
 * Class PlateauCommander
 * @package logic\commands
 */
class PlateauCommander extends AbsCommander
{
    const GROUP_COMMANDS = 1;
}
