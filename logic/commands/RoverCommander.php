<?php

namespace logic\commands;

use infra\OutputFormatter;

/**
 * Class RoverCommander
 * @package logic\commands
 */
class RoverCommander extends AbsCommander
{
    const GROUP_COMMANDS = 2;

    public function executeCommands()
    {
        $response = parent::executeCommands();
        if ($response) {
            $newCoords = $this->receiver->getCoordinates();
            $newOrientation = $this->receiver->getOrientation();

            $output = new OutputFormatter();
            return $output->formatRoverResponse([$newCoords->getPosX(), $newCoords->getPosY(), $newOrientation]);
        }

        return $response;
    }
}
