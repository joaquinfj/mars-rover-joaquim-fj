<?php

namespace logic\mars;

/**
 * Class Coordinates
 * @package logic\mars
 */
class Coordinates
{
    /** @var integer $posX */
    protected $posX;
    /** @var integer $posY */
    protected $posY;

    /**
     * Coordinates constructor.
     * @param int $posX
     * @param int $posY
     */
    public function __construct($posX, $posY)
    {
        return $this->setNewCoordinates($posX, $posY);
    }

    public function setNewCoordinates($posX, $posY)
    {
        $this->setPosX($posX);
        $this->setPosY($posY);
        return $this;
    }

    /**
     * @return int
     */
    public function getPosX()
    {
        return $this->posX;
    }

    /**
     * @param int $posX
     */
    protected function setPosX($posX)
    {
        $this->posX = $posX;
    }

    /**
     * @return int
     */
    public function getPosY()
    {
        return $this->posY;
    }

    /**
     * @param int $posY
     */
    protected function setPosY($posY)
    {
        $this->posY = $posY;
    }
}
