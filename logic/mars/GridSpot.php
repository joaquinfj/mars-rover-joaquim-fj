<?php

namespace logic\mars;

/**
 * Class GridSpot
 * @package logic\mars
 */
class GridSpot
{
    /** @var  Coordinates $coordinates */
    protected $coordinates;
    /** @var  MarsElement|bool $occupier */
    protected $occupier;

    /** var int $photographed */
    /** protected $photographed; */

    /**
     * GridSpot constructor.
     * @param Coordinates $coordinates
     */
    public function __construct(Coordinates $coordinates)
    {
        $this->setCoordinates($coordinates);
        $this->setOccupier(false);
    }

    /**
     * @return Coordinates
     */
    public function getCoordinates()
    {
        return $this->coordinates;
    }

    /**
     * @param Coordinates $coordinates
     */
    public function setCoordinates($coordinates)
    {
        $this->coordinates = $coordinates;
    }

    /**
     * @return bool|IMarsElement
     */
    public function getOccupier()
    {
        return $this->occupier;
    }

    /**
     * @param bool|MarsElement $occupier
     * @return bool|MarsElement
     */
    public function setOccupier($occupier)
    {
        $this->occupier = $occupier;
        return $this->occupier;
    }
}
