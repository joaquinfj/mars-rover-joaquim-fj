<?php

namespace logic\mars;

/**
 * Interface IMarsElement
 * @package logic\mars
 */
interface IMarsElement
{
    /**
     * @return string
     */
    public function getType();

    /**
     * @param string $command
     * @return mixed
     */
    public function executeCommand($command);
}
