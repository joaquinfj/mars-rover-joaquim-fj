<?php
/**
 * Created by PhpStorm.
 * User: joaquim
 * Date: 20/11/16
 * Time: 15:00
 */

namespace logic\mars;

/**
 * Class MarsElement
 * @package logic\mars
 */
abstract class MarsElement implements IMarsElement
{
    /** @var  string $name */
    protected $name;
    /** @var  string $type */
    protected $type;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return Coordinates
     */
    abstract public function getCoordinates();
}
