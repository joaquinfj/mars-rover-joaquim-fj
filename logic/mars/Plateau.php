<?php

namespace logic\mars;

use infra\ErrorHandler;

/**
 * Class Plateau
 * @package logic\mars
 */
class Plateau extends MarsElement
{
    /** @var  Coordinates $bottomCoordinates */
    protected $bottomCoordinates;
    /** @var  Coordinates $topCoordinates */
    protected $topCoordinates;
    /** @var  GridSpot[][] $aSpots */
    protected $aSpots;

    /**
     * Plateau constructor.
     */
    public function __construct()
    {
        $this->name = 'Unique plateau';
        $this->type = 'Plateau';
        $this->bottomCoordinates = new Coordinates(0, 0);
        $this->topCoordinates = new Coordinates(0, 0);
        $this->aSpots = array();
    }

    /**
     * @return string
     */
    public function commandRulesValidation()
    {
        return "/([1-9]) ([1-9])/";
    }

    /**
     * @param string $command
     * @return string
     */
    public function executeCommand($command)
    {
        $command =$this->parseCommand($command);
        if (!$command) {
            return ErrorHandler::returnError('Plateau command not accepted.');
        }

        list($topX, $topY) = $command;
        if (!$this->setDimensions($topX, $topY)) {
            return ErrorHandler::returnError('Dimensions could not be set.');
        }

        return '';
    }

    /**
     * @param string $command
     * @return array|bool
     */
    protected function parseCommand($command)
    {
        $pattern = $this->commandRulesValidation();
        $countMatches = preg_match($pattern, $command, $matches);
        if ($countMatches!==1) {
            return false;
        }
        $posX = intval($matches[1]);
        $posY = intval($matches[2]);
        return array($posX, $posY);
    }

    /**
     * @param int $topX
     * @param int $topY
     * @return Coordinates
     */
    protected function setDimensions($topX, $topY)
    {
//        $this->bottomCoordinates = new Coordinates(1, 1);
        $this->topCoordinates = new Coordinates($topX, $topY);

        /** from left to right, from bottom to top */
        for ($posY = $this->bottomCoordinates->getPosY(); $posY <= $this->topCoordinates->getPosY(); $posY++) {
            for ($posX = $this->bottomCoordinates->getPosX(); $posX <= $this->topCoordinates->getPosX(); $posX++) {
                $gridSpot = new GridSpot(new Coordinates($posX, $posY));
                $this->aSpots[$posX][$posY] = $gridSpot;
            }
        }

        return $this->topCoordinates;
    }

    /**
     * @param Coordinates $coordinates
     * @return bool
     */
    public function isSpotFree(Coordinates $coordinates)
    {
        $posX = $coordinates->getPosX();
        if ($posX<$this->bottomCoordinates->getPosX() ||
            $coordinates->getPosY()<$this->bottomCoordinates->getPosX() ||
            $coordinates->getPosX()>$this->topCoordinates->getPosX() ||
            $coordinates->getPosX()>$this->topCoordinates->getPosY() ) {
            return ErrorHandler::returnError('Error: Off the limits.');
        }

        if (array_key_exists($posX, $this->aSpots)) {
            if (!array_key_exists($coordinates->getPosY(), $this->aSpots[$posX])) {
                return ErrorHandler::returnError('Error: This coordinate does not exist in this plateau.');
            }
        }

        if ($this->aSpots[$coordinates->getPosX()][$coordinates->getPosY()]->getOccupier()===false) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param Coordinates $coordinates
     * @param MarsElement|null $object
     * @return bool
     */
    public function setOccupancy(Coordinates $coordinates, $object = null)
    {
        $toOccupy = true;
        if (!isset($object) || $object===false) {
            $toOccupy = false;
        }
        if ($toOccupy && !$this->isSpotFree($coordinates)) {
            return ErrorHandler::returnError('Could not be set. The object '.
                'of type ' . $object->getType() . ' is there.');
        }
        if (!isset($object)) {
            $object = false;
        }

        $this->aSpots[$coordinates->getPosX()][$coordinates->getPosY()]->setOccupier($object);
        return true;
    }

    /**
     * @return Coordinates
     */
    public function getCoordinates()
    {
        return $this->bottomCoordinates;
    }
}
