<?php

namespace logic\mars;

use infra\ErrorHandler;

/**
 * Class Rover
 * @package logic\mars
 */
class Rover extends MarsElement
{
    const CMD_TYPE_SPIN = 'spin';
    const CMD_TYPE_MOVE = 'move';
    const CMD_TYPE_LAND = 'land';

    /** @var int $speed */
    protected $speed = 1;
    /** @var  RoverCompass $compass */
    protected $compass;
    /** @var Coordinates $coordinates */
    protected $coordinates;
    /** @var  string $orientation */
    protected $orientation;
    /** @var  Plateau $plateau */
    protected $plateau;

    /**
     * Plateau constructor.
     */
    public function __construct()
    {
        $this->type = 'Rover';
        $this->compass = new RoverCompass();
        $this->coordinates = new Coordinates(0, 0);
        $this->plateau = null;
        $this->orientation = null;
    }

    /**
     * @return array
     */
    public function commandRulesValidation()
    {
        $rules = array();
        $rules[$this::CMD_TYPE_LAND] = "/([1-9]) ([1-9]) ([N,E,S,W])/";
        $rules[$this::CMD_TYPE_MOVE.'-'.$this::CMD_TYPE_SPIN] = "/([L,M,R]){1}/";
        return $rules;
    }

    /**
     * @param string $command
     * @return array|bool
     */
    protected function parseCommand($command)
    {
        $patterns = $this->commandRulesValidation();
        $commandType = '';
        $anyMatch = false;
        foreach ($patterns as $keyCmdType => $pattern) {
            $countMatches = preg_match_all($pattern, $command, $matches);
            if ($countMatches>=1) {
                $commandType = $keyCmdType;
                $anyMatch = true;
                break;
            }
        }

        if (!$anyMatch) {
            return false;
        }

        array_shift($matches);
        return array('commandType'=>$commandType, 'operations'=>$matches);
    }

    /**
     * @param string $command
     * @return bool
     */
    public function executeCommand($command)
    {
        $command =$this->parseCommand($command);
        if (!$command) {
            return ErrorHandler::returnError('Plateau command not accepted.');
        }

        switch ($command['commandType']) {
            case $this::CMD_TYPE_LAND:
                $posX = $command['operations'][0];
                $posY = $command['operations'][1];
                $orientation = $command['operations'][2];
                $this->land($posX[0], $posY[0], $orientation[0]);
                break;
            case $this::CMD_TYPE_MOVE.'-'.$this::CMD_TYPE_SPIN:
                foreach ($command['operations'][0] as $operation) {
                    if ($this->compass->isValidRotation($operation)) {
                        $this->spin($operation);
                    } else {
                        $this->move();
                    }
                }
                break;

            default:
                return ErrorHandler::returnError('Command group in line not implemented.');
                break;
        }

        return true;
    }

    /**
     * @return Coordinates
     */
    public function getCoordinates()
    {
        return $this->coordinates;
    }

    /**
     * @param $coordinates
     * @return Coordinates
     */
    public function setCoordinates($coordinates)
    {
        $this->coordinates = $coordinates;
        return $this->coordinates;
    }

    /**
     * @return string
     */
    public function getOrientation()
    {
        return $this->orientation;
    }

    /**
     * @param string $orientation
     */
    public function setOrientation($orientation)
    {
        $this->orientation = $orientation;
    }

    /**
     * @return Plateau
     */
    public function getPlateau()
    {
        return $this->plateau;
    }

    /**
     * Subscriber
     * @param MarsElement $plateau
     */
    public function setPlateau($plateau)
    {
        $this->plateau = $plateau;
    }

    /**
     * @return bool
     */
    protected function move()
    {

        $currentCoords = $this->getCoordinates();
        $newCoords = clone($currentCoords);
        $navigation = $this->compass;
        switch ($this->getOrientation()) {
            case $navigation::NORTH:
                $newCoords->setNewCoordinates($currentCoords->getPosX(), $currentCoords->getPosY()+$this->speed);
                break;
            case $navigation::EAST:
                $newCoords->setNewCoordinates($currentCoords->getPosX()+$this->speed, $currentCoords->getPosY());
                break;
            case $navigation::SOUTH:
                $newCoords->setNewCoordinates($currentCoords->getPosX(), $currentCoords->getPosY()-$this->speed);
                break;
            case $navigation::WEST:
                $newCoords->setNewCoordinates($currentCoords->getPosX()-$this->speed, $currentCoords->getPosY());
                break;
        }

        if (!$this->plateau->isSpotFree($newCoords)) {
            return ErrorHandler::returnError('Spot is occupied by another object.');
        }

        $oldCoordinates = $currentCoords;
        $this->notifyPlateau($oldCoordinates, false);
        $this->notifyPlateau($this->setCoordinates($newCoords), $this);
        return true;
    }

    /**
     * @param string $spinMove
     * @return bool|string
     */
    protected function spin($spinMove)
    {
        $newOrientation = $this->compass->rotate($this->getOrientation(), $spinMove);
        if (!$newOrientation) {
            return ErrorHandler::returnError('Could not execute rotation/spin towards '.$spinMove);
        }

        $this->setOrientation($newOrientation);
        return $newOrientation;
    }

    /**
     * @param string $posX
     * @param string $posY
     * @param string $orientation
     * @return bool
     */
    protected function land($posX, $posY, $orientation)
    {
        $this->name = 'Rover-Ini-At-'.$posX.$posY;

        $newCoords = new Coordinates(intval($posX), intval($posY));
        if (!$this->plateau->isSpotFree($newCoords)) {
            return ErrorHandler::returnError('Spot to land busy.');
        }

        $this->setOrientation($orientation);
        $this->notifyPlateau($this->setCoordinates($newCoords), $this);
        return true;
    }

    /**
     * @param Coordinates $coords
     * @param Rover|bool $object
     * @return bool
     */
    protected function notifyPlateau(Coordinates $coords, $object = false)
    {
        if (!$this->plateau->setOccupancy($coords, $object)) {
            return ErrorHandler::returnError('Object Rover could not be placed into plateau.');
        }

        return true;
    }
}
