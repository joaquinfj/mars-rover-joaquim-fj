<?php

namespace logic\mars;

use infra\ErrorHandler;

/**
 * Class Compass
 * @package logic\mars
 */
class RoverCompass
{
    const NORTH = 'N';
    const EAST = 'E';
    const SOUTH = 'S';
    const WEST = 'W';

    const LEFT = 'L';
    const RIGHT = 'R';

    protected $possibleRotations = [self::LEFT=>'-90', self::RIGHT=>'90'];
    protected $possibleOrientations = [
        '0'=>self::NORTH,
        '360'=>self::NORTH,
        '90'=>self::EAST,
        '180'=>self::SOUTH,
        '270'=>self::WEST];

    /**
     * @param string $curOrientation
     * @param string $rotation
     * @return string
     */
    public function rotate($curOrientation, $rotation)
    {
        if (!$this->isValidRotation($rotation)) {
            return ErrorHandler::returnError('The rotation '.$rotation.' is not allowed.');
        }
        if (!$this->isValidOrientation($curOrientation)) {
            return ErrorHandler::returnError('The current orientation '.$curOrientation.' is not allowed.');
        }

        $idxOrientation = array_search($curOrientation, array_values($this->possibleOrientations));
        $aDegrees = array_keys($this->possibleOrientations);
        $degreesNow = intval($aDegrees[$idxOrientation]);
        if ($degreesNow==0 && $rotation==$this::LEFT) {
            $degreesNow=360;
        }

        $endRotation = $degreesNow + intval($this->possibleRotations[$rotation]);
        return $this->possibleOrientations[strval($endRotation)];
    }

    /**
     * @param string $orientation
     * @return bool
     */
    public function isValidOrientation($orientation)
    {
        if (array_search($orientation, array_values($this->possibleOrientations))===false) {
            return false;
        }

        return true;
    }

    /**
     * @param string $rotation
     * @return bool
     */
    public function isValidRotation($rotation)
    {
        if (array_search($rotation, array_keys($this->possibleRotations))===false) {
            return false;
        }

        return true;
    }
}
