#!/usr/bin/env php
<?php
//echo "Sending NASA commands to Input commander in Mars...";

require(__DIR__ . '/vendor/autoload.php');
const EXIT_CODE_NORMAL = 0;
const EXIT_CODE_ERROR = 1;

$application = new \logic\commands\InputCommandParser();
$response = $application->processInputCommands($argv);
if ($response===false) {
    exit(EXIT_CODE_ERROR);
}

echo $response;
exit(EXIT_CODE_NORMAL);
