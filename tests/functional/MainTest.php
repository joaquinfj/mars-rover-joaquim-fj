<?php

namespace tests\functional;

use infra\ErrorHandler;

/**
 * Class MainTest
 */
class MainTest extends \PHPUnit_Framework_TestCase
{
    protected function setUp()
    {
        parent::setUp();
        // Only useful in case of invoking through PHP UNIT TEST, not in functional.
        ErrorHandler::setExceptionHandling(false);
    }

    /**
     * @test
     * @covers main.php
     */
    public function testSimpleSuccessMain()
    {
        // LMLMLMM
        $input = <<<ECHO
        "5 5
        1 2 N
        LM"
ECHO;

        $expectedOutput = "0 2 W";
//        $expectedOutput .= "5 1 E" . PHP_EOL;

        $response = shell_exec('php5 /var/www/mars-rover-joaquim-fj/main.php '.$input. '');
        $this->assertEquals($expectedOutput, $response, 'The output of the main script is different from expected.');

        return true;
    }

    /**
     * @test
     * @covers main.php
     */
    public function testMainOriginal()
    {
        $input = <<<ECHO
        "5 5
        1 2 N
        LMLMLMLMM
        3 3 E
        MMRMMRMRRM"
ECHO;
        
        $expectedOutput = "1 3 N" . PHP_EOL;
        $expectedOutput .= "5 1 E";

        $response = shell_exec('php5 /var/www/mars-rover-joaquim-fj/main.php '.$input. '');
        $this->assertEquals($expectedOutput, $response, 'The output of the main script is different from expected.');

        return true;
    }

    /**
     * @test
     * @covers main.php
     */
    public function testErrMainScript1RoverOffLimits()
    {
        $input = <<<ECHO
        "5 5
        5 5 N
        M"
ECHO;


        $notExpected = '5 6 N';
        $response = shell_exec('php5 /var/www/mars-rover-joaquim-fj/main.php '.$input. '');

        $this->assertNotEquals($notExpected, $response, 'Should not allow this move.');
        $this->assertGreaterThanOrEqual(0, strpos($response, 'Error'), 'Should not allow this, error ex expected.');

        return true;
    }

    /**
     * @test
     * @covers main.php
     */
    public function testMainMultipleRovers()
    {
        $input = <<<ECHO
        "6 6
        5 5 S
        MMMR
        4 4 W
        MML        
        1 1 W
        RMRM"
ECHO;

        $expectedOutput = "5 2 W" . PHP_EOL;
        $expectedOutput .= "2 4 S" . PHP_EOL;
        $expectedOutput .= "2 2 E";

        $response = shell_exec('php5 /var/www/mars-rover-joaquim-fj/main.php '.$input. '');
        $this->assertEquals($expectedOutput, $response, 'The output of the main script is different from expected.');

        return true;
    }

    /**
     * @test
     * @covers main.php
     */
    public function testErrorMainRoverOverlandOverRover()
    {
        $input = <<<ECHO
        "5 5
        5 5 S
        MLM
        4 4 N
        MML"
ECHO;

        $response = shell_exec('php5 /var/www/mars-rover-joaquim-fj/main.php '.$input. '');
        $this->assertGreaterThanOrEqual(0, strpos($response, 'Error'), 'Should not allow this, error ex expected.');
        $this->assertGreaterThanOrEqual(
            0,
            strpos($response, 'Off the limits'),
            'Should not allow this, error ex expected.'
        );

        return true;
    }

    /**
     * @test
     * @covers main.php
     */
    public function testErrorMainRoverCrashOverRover()
    {
        $input = <<<ECHO
        "5 5
        5 5 S
        MM
        5 3 N
        MM"
ECHO;

        $response = shell_exec('php5 /var/www/mars-rover-joaquim-fj/main.php '.$input. '');
        $this->assertGreaterThanOrEqual(
            0,
            strpos($response, 'Spot to land busy'),
            'Should not allow this, error was expected.'
        );

        return true;
    }
}
